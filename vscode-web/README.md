# VS Code Web

An SCSS fork of Coder's `VS Code Web` terraform module that adds the following features:

 - Specify a workspace to open instead of a simple folder (using `workspace=` variable to specify a `.code-workspace` file)
 - Specifies VSCode settings to be applied using a JSON string instead of a map, providing greater flexibility
 - Merges specified VSCode settings with any existing settings, instead of simply replacing all settings
 - Install all specified extensions with one invocation of the vscode command, improving performance
 - Install extensions oneline even when USE_CACHED=true

Automatically installs [Visual Studio Code Server](https://code.visualstudio.com/docs/remote/vscode-server) in a workspace and create an app to access it via the dashboard.

```tf
module "vscode-web" {
  source         = "registry.coder.com/modules/vscode-web/coder"
  version        = "1.0.22"
  agent_id       = coder_agent.example.id
  accept_license = true
}
```


## Examples

### Install VS Code Web to a custom folder

```tf
module "vscode-web" {
  source         = "registry.coder.com/modules/vscode-web/coder"
  version        = "1.0.22"
  agent_id       = coder_agent.example.id
  install_prefix = "/home/coder/.vscode-web"
  folder         = "/home/coder"
  accept_license = true
}
```

### Install Extensions

```tf
module "vscode-web" {
  source         = "registry.coder.com/modules/vscode-web/coder"
  version        = "1.0.22"
  agent_id       = coder_agent.example.id
  extensions     = ["github.copilot", "ms-python.python", "ms-toolsai.jupyter"]
  accept_license = true
}
```

### Pre-configure Settings

Configure VS Code's [settings.json](https://code.visualstudio.com/docs/getstarted/settings#_settingsjson) file:

```tf
module "vscode-web" {
  source     = "registry.coder.com/modules/vscode-web/coder"
  version    = "1.0.22"
  agent_id   = coder_agent.example.id
  extensions = ["dracula-theme.theme-dracula"]
  settings = {
    "workbench.colorTheme" = "Dracula"
  }
  accept_license = true
}
```
